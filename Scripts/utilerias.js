var hostWebApi = "http://www.incidenciasrh.col.gob.mx/"; //"http://localhost/";
var nameWebApi = "ReqPerWebApi";
var methodPersDescatalogado = hostWebApi + nameWebApi + "/getPersonalDescatalogadoPost";

function mayusculasTextBox() {
    $("input[type=text]").each(function () {
        $(this).attr('onchange', 'javascript:this.value=this.value.toUpperCase();');
        $(this).attr('autocomplete', 'off');
        //$(this).attr('onpaste', 'return false;');
        //$(this).attr('oncopy', 'return false;');
        // $(this).attr('onkeypress', 'return validar(event)');
    });
    $("textarea").each(function () {
        $(this).attr('onchange', 'javascript:this.value=this.value.toUpperCase();');
        //$(this).attr('onkeypress', 'return validar(event)');
        $(this).attr('autocomplete', 'off');
        //$(this).attr('onpaste', 'return false;');
        //$(this).attr('oncopy', 'return false;');
    });
}
function CargarAutoNumeric(txt) {
    $(txt).autoNumeric("init", {
        digitGroupSeparator: '.',
        decimalCharacter: ',',
        currencySymbol: '� '
    });
}
function CargarDatePicker(txt) {
    console.log("datepick");
    $(txt).datepicker({ changeMonth: true, changeYear: true, dateFormat: "dd/mm/yy",
        dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
        monthNamesShort: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        prevText: "Anterior", nextText: "Siguiente"
    });
    $(txt).keydown(function (e) {
        if (e.keyCode == 27) {
            // Prevent default (disable the back button behavior)
            e.preventDefault();
            $(txt).val("");
            // Your code
        } else {
            return false;
        }
    });
}
function CargarTimePicker(txt) {
    $(txt).timeEntry({ show24Hours: true });
}
function CargarContenidoLoad(zona, action, parametros, codigo) {
    //'<%: Url.Action("Solicitud_ListaEscuelas", "Solicitud") %>' + '?crud=' + $("#hdnCRUD").val()
    $(zona).load(action, parametros, function () {
        eval(codigo)
    });
}
function CargarCapaBlok(div) {
    var posicionReal = $("#" + div).offset();
    var Divtop = posicionReal.top;
    var DivLeft = posicionReal.left;
    var capa = $("#" + div);
    var WidthDiv = capa.css("width");
    WidthDiv = WidthDiv.replace("px", "");
    WidthDiv = Number(WidthDiv) + (WidthDiv * .03);
    WidthDiv = WidthDiv + "px";
    var HeightDiv = capa.css("height");
    HeightDiv = HeightDiv.replace("px", "");
    HeightDiv = Number(HeightDiv) + (HeightDiv * .11);
    HeightDiv = HeightDiv + "px";
    var dimensiones = "";
    dimensiones += "Dimensiones internas: " + capa.innerWidth() + "x" + capa.innerHeight();
    dimensiones += "\nDimensiones externas: " + capa.outerWidth() + "x" + capa.outerHeight();
    $("body").append("<div id='Desactivar' style='z-index:1000; clear:both; overflow:scroll; position: absolute; top:" + Divtop + "px; left:" + DivLeft + "px; height:" + HeightDiv + "; width:" + WidthDiv + "; solid #FF6666; background-color:#000000; color:#FFFFFF;  filter:alpha(opacity=0); -moz-opacity: 0.01; opacity: 0.10;'></div>");
    $("body").css("overflow", "hidden");
}
function ModalTynyBox(url, w, h, codigo) {
    //'<%:Url.Action("Mantenimiento_Paciente","Paciente")%>'
    TINY.box.show({ url: url, width: w, height: h, openjs: function () {
        mayusculasTextBox();
        eval(codigo);

    }
    });
}
function CerrarTINY() {
    TINY.box.hide();
    $("body").css("overflow", "auto");
}
function CerrarModalDyn() {
    $("#dVentanaDyn").remove();
    $("#capaModalD").remove();
    $("#Desactivar").remove();
}
function CerrarModalCon() {
    $("#dVentanaCon").remove();
    $("#capaModalC").remove();
}
function ModalDynamica(w, h, ml, top, action, parametros, codigo) {

    $("body").append("<div id='capaModalD'></div>");
    $("body").append("<div id='dVentanaDyn'></div>");
    $("#dVentanaDyn").css("width", w + "%");
    $("#dVentanaDyn").css("height", h + "%");
    $("#dVentanaDyn").css("margin-left", ml + "%");
    $("#dVentanaDyn").css("top", top + "%");
    $("#dVentanaDyn").append("<div id='closeMD' onclick='CerrarModalDyn();'></div>" +
        "<div id='contentMD' style='width:90%;margin:2% auto auto;'></div>");
    var Ventana = document.getElementById("contentMD");
    url = "../Content/css/tinybox/images/preload.gif";
    Ventana.innerHTML = '<br /><center><img style="width:7%" src="' + url + '" /></img></center>';
    CargarContenidoLoad($("#contentMD"), action, parametros, codigo);

}
function ModalConfirmacion(texto, color, aceptar) {

    $("body").append("<div id='capaModalC'></div>");
    $("body").append("<div id='dVentanaCon'></div>");
    $("#dVentanaCon").css("width", "20%");
    $("#dVentanaCon").css("height", "23%");
    $("#dVentanaCon").css("margin-left", "40%");
    $("#dVentanaCon").css("top", "40%");
    $("#dVentanaCon").css("background-color", color);
    $("#dVentanaCon").append("<div id='closeMC' onclick='CerrarModalCon();'></div>" +
        "<div id='contentMC' style='width:90%;margin:7% auto auto;'></div>");
    var Ventana = document.getElementById("contentMC");
    Ventana.innerHTML = '<center><span style="font-weight:bold">' + texto + '</span>' +
        '<div style="margin-top:5px;margin-bottom:5px"><span>Finado: </span><input type="checkbox" onclick="ActualizarFinado(this.checked)" /></div>' +
        '<button type="button" class="btn btn-danger" onclick="' + aceptar + '">Aceptar</button>' +
        '<button type="button" class="btn" style="margin-left:5px" onclick="CerrarModalCon()">Cancelar</button></center>';

}
function soloNumerosEnteros(input) {
    //alert("ax");
    //console.log("ax");
    $(input).keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        //alert(e.keyCode);
        //console.log(e.keyCode);
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 || //190 punto
        // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
        // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39) ||
            (e.keyCode >= 96 && e.keyCode <= 105)
            ) {
            // let it happen, don't do anything
            //console.log("teclas numericas");
            if (e.keyCode == 96 && $(input).val() == "") {
                return false;
            } else {
                return;
            }
        } else if (e.keyCode == 48 && $(input).val() == "") {
            //console.log("entra");
            //console.log("cero inicial");
            return false;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 97 || e.keyCode > 105)) {
            //console.log("teclas especiales");
            e.preventDefault();
        }
    });
}