var app = angular.module('rdpApp', ['localytics.directives', 'angular-js-xlsx']);

app.service('filteredListService', function () {

    this.searched = function (valLists, toSearch) {
        return _.filter(valLists,

        function (i) {
            /* Search Text in all 3 fields */
            return searchUtil(i, toSearch);
        });
    };

    this.paged = function (valLists, pageSize) {
        retVal = [];
        for (var i = 0; i < valLists.length; i++) {
            if (i % pageSize === 0) {
                retVal[Math.floor(i / pageSize)] = [valLists[i]];
            } else {
                retVal[Math.floor(i / pageSize)].push(valLists[i]);
            }
        }
        return retVal;
    };

});

app.controller('pageOne', function($scope, $http, $filter, filteredListService) {
    
    $scope.urlBase = "http://www.incidenciasrh.col.gob.mx/reqperwebapi/";
    $scope.cveDependencia = 'sie';
    $scope.default = [];
    $scope.firstName = "John";
    $scope.lastName = "Doe";
    $scope.codType = '';
    $scope.codPosition = '';
    $scope.codArea = '';
    $scope.codReason = '';
    $scope.codPositionType = '';
    $scope.auxType = '';
    $scope.loading = false;
    
    $scope.nameCandidate = '';
    $scope.lastFCandidate = '';
    $scope.lastMCandidate = '';
    
    $http.post($scope.urlBase+"GetValoresAccesoPost", {cveDependencia: $scope.cveDependencia}).then(function(data, status) {
          $scope.default = data.data;
    })

    $http.post($scope.urlBase+"GetValoresReferenciaPost", {cveValor : ''}).then(function(data, status) {
            $scope.positions = data.data;
    })
    
    $http.post($scope.urlBase+"GetValoresReferenciaPost", {cveValor : 'TIPOREQ'}).then(function(data, status) {
        $scope.types = data.data;
    })

    $http.post($scope.urlBase+"GetValoresReferenciaPost", {cveValor : 'DEPEND'}).then(function(data, status) {
        $scope.dependencies = data.data;
    })
    
    $http.post($scope.urlBase+"GetValoresReferenciaPost", {cveValor : 'DIRECC'}).then(function(data, status) {
        $scope.areas = data.data;
    })

    $http.post($scope.urlBase+"GetValoresReferenciaPost", {cveValor : 'MTVOSOLNI'}).then(function(data, status) {
        $scope.reasons = data.data;
    })

    $http.post($scope.urlBase+"GetValoresReferenciaPost", {cveValor : ''}).then(function(data, status) {
        $scope.positionTypes = data.data;
    })

    $scope.setSustitution = function(){
        $http.post($scope.urlBase+"GetValoresReferenciaPost", {cveValor : 'PUESNOMSUP'}).then(function(data, status) {
            $scope.positions = data.data;
        })
    }

    $scope.getOtherTypes = function(){

        if($scope.codType == 1){
           $scope.auxType = 'MTVOSOLNI';
        }else if($scope.codType == 2){
           $scope.auxType = 'MTVOSOLSUP';
           $scope.setSustitution();
        }else if($scope.codType == 3){
           $scope.auxType = 'MTVOSOLCORR';
        }else{
           $scope.auxType = '';
        }


        $http.post($scope.urlBase+"GetValoresReferenciaPost", {cveValor : $scope.auxType}).then(function(data, status) {
            $scope.positionTypes = data.data;
        })
    }

    $scope.sendRequisition = function(){
        $scope.loading = true;
        $scope.vali = true;

        $('.error').remove();

        if($scope.codType == ''){
            $('.codType').closest('.form-inline').append('<p class="error">Este campo es requerido</p>');
            $scope.vali = false;
        }

        if($scope.codDependency == ''){
            $('.codDependency').closest('.form-inline').append('<p class="error">Este campo es requerido</p>');
            $scope.vali = false;
        }

        if($scope.codArea == ''){
            $('.codArea').closest('.form-inline').append('<p class="error">Este campo es requerido</p>');
            $scope.vali = false;
        }

        if($scope.codPosition == ''){
            $('.codPosition').closest('.form-inline').append('<p class="error">Este campo es requerido</p>');
            $scope.vali = false;
        }

        if($scope.codFunctionalPosition == ''){
            $('.codFunctionalPosition').closest('.form-inline').append('<p class="error">Este campo es requerido</p>');
            $scope.vali = false;
        }

        if($scope.codReason == ''){
            $('.codReason').closest('.form-inline').append('<p class="error">Este campo es requerido</p>');
            $scope.vali = false;
        }

        if($scope.codPositionType == ''){
            $('.codPositionType').closest('.form-inline').append('<p class="error">Este campo es requerido</p>');
            $scope.vali = false;
        }

        if($scope.codExperience == ''){
            $('.codExperience').closest('.form-inline').append('<p class="error">Este campo es requerido</p>');
            $scope.vali = false;
        }

        if($scope.codFunctions == ''){
            $('.codFunctions').closest('.form-inline').append('<p class="error">Este campo es requerido</p>');
            $scope.vali = false;
        }

        if($scope.default.Req_JefeInmediato == ''){
            $('.Req_JefeInmediato').closest('.form-inline').append('<p class="error">Este campo es requerido</p>');
            $scope.vali = false;
        }

        if($scope.default.Req_CargoJefe == ''){
            $('.Req_CargoJefe').closest('.form-inline').append('<p class="error">Este campo es requerido</p>');
            $scope.vali = false;
        }

        if($scope.default.Req_Coordinador == ''){
            $('.Req_Coordinador').closest('.form-inline').append('<p class="error">Este campo es requerido</p>');
            $scope.vali = false;
        }

        if($scope.default.Req_Secretario == ''){
            $('.Req_Secretario').closest('.form-inline').append('<p class="error">Este campo es requerido</p>');
            $scope.vali = false;
        }

        if($scope.default.Req_Capturista == ''){
            $('.Req_Capturista').closest('.form-inline').append('<p class="error">Este campo es requerido</p>');
            $scope.vali = false;
        }

        if($scope.default.Req_Correo == ''){
            $('.Req_Correo').closest('.form-inline').append('<p class="error">Este campo es requerido</p>');
            $scope.vali = false;
        }

        if($scope.default.Req_Telefono == ''){
            $('.Req_Telefono').closest('.form-inline').append('<p class="error">Este campo es requerido</p>');
            $scope.vali = false;
        }     
        

        if($scope.vali == false){
            $scope.loading = false;
            return false;
        }

        $scope.data = {
              "idRequisicion": 0,
              "folio": null,
              "fechaCaptura": new Date(),
              "tipo": $scope.codType,
              "oficioSindicato": null,
              "idDependencia": $scope.codDependency,
              "idDireccion": $scope.codArea,
              "puestoNominalActual": null,
              "puestoNominal": null,
              "puestoFuncionalActual": null,
              "puestoFuncional": $scope.codFunctionalPosition,
              "idMotivo": $scope.codReason,
              "idTipoPuesto": $scope.codPositionType,
              "experienciaRequerida": $scope.codExperience,
              "funcionesARealizar": $scope.codFunctions,
              "jefeInmediato": $scope.default.Req_JefeInmediato,
              "cargoJefe": $scope.default.Req_CargoJefe,
              "coordinadorAdmvo": $scope.default.Req_Coordinador,
              "secretario": $scope.default.Req_Secretario,
              "capturista": $scope.default.Req_Capturista,
              "correoCapt": $scope.default.Req_Correo,
              "telefono": $scope.default.Req_Telefono,
              "estatus": 0,
              "fechaActualizacion": new Date(),
              "usrAcceso": 'sie',
              "items": $scope.candidates,
        }

        $http.post($scope.urlBase+"insertRequisicionPost", $scope.data)
        .then(function(data, status) {
           $scope.loading = false;
        });
    }
    
    $scope.candidates = []
    
    $scope.addCandidate = function(){
        $scope.candidates.push({
            "idRequisicion": 0,
             "idConsecutivo": ($scope.candidates.length + 1),
             "nombre": $scope.nameCandidate,
             "apPaterno": $scope.lastFCandidate,
             "apMaterno": $scope.lastMCandidate,
             "noControl": null,
        });
        
        $scope.nameCandidate = '';
        $scope.lastFCandidate = '';
        $scope.lastMCandidate = '';

        console.log($scope.candidates)        
        
        $('#myModal').modal('hide');
    }
    $scope.deleteCandidate = function(index){
        $scope.candidates.splice(index, 1);

        for (var i = 0; i < $scope.candidates.length; i++) {
            $scope.candidates[i].idConsecutivo = i+1;
        }

        console.log($scope.candidates)
    }

    $scope.read = function (workbook) {

                var headerNames = XLSX.utils.sheet_to_json( workbook.Sheets[workbook.SheetNames[0]], { header: 1 })[0];
                var data = XLSX.utils.sheet_to_json( workbook.Sheets[workbook.SheetNames[0]]);

                console.log(headerNames);
                console.log(data);

                /*for (var row in data)
                {
                    Object.keys(data[row]).forEach(function(key) {
                        console.log("Key = >" + key);
                        console.log("Value => " + data[row][key]);
                        console.log("===========================");
                    });
                }*/
            }

            $scope.error = function (e) {
                console.log(e);
            }
});

app.controller('pageTwo', function($scope, $http, $filter, filteredListService) {
    $scope.firstName = "John";
    $scope.lastName = "Doe";
    
    $scope.cveDependencia = 'sie'//$("#hdnCveDependencia").val();
    
    $scope.urlBase = "http://www.incidenciasrh.col.gob.mx/reqperwebapi/";
    
    $http.post($scope.urlBase+"getAllRequisionPost", {cveDependencia: $scope.cveDependencia}).then(function(data, status) {
          console.log(data)
          $scope.allItems = data.data;
          //$scope.sort('IdRequisicion');   
          //$scope.pagination();
    })
    
    
    $scope.pageSize = 6;
  
    $scope.reverse = false;

    $scope.resetAll = function () {
        $scope.filteredList = $scope.allItems;
        $scope.newEmpId = '';
        $scope.newName = '';
        $scope.newEmail = '';
        $scope.searchText = '';
        $scope.currentPage = 0;
        $scope.Header = ['', '', ''];
    }
    
    $scope.search = function () {
        $scope.filteredList = filteredListService.searched($scope.allItems, $scope.searchText);
 
        if ($scope.searchText == '') {
            $scope.filteredList = $scope.allItems;
        }
        $scope.pagination();
    }

    
    $scope.pagination = function () {
        $scope.ItemsByPage = filteredListService.paged($scope.filteredList, $scope.pageSize);
    };
 
    $scope.setPage = function () {
        $scope.currentPage = this.n;
    };
 
    $scope.firstPage = function () {
        $scope.currentPage = 0;
    };
 
    $scope.lastPage = function () {
        $scope.currentPage = $scope.ItemsByPage.length - 1;
    };
    
    $scope.range = function (input, total) {
        var ret = [];
        if (!total) {
            total = input;
            input = 0;
        }
        
        for (var i = input; i < total; i++) {
            if (i != 0 && i != total - 1) {
                ret.push(i);
            }
        }
        return ret;
    };
    
    $scope.sort = function (sortBy) {
        $scope.resetAll();
 
        $scope.columnToOrder = sortBy;
 
        //$Filter - Standard Service
        $scope.filteredList = $filter('orderBy')($scope.filteredList, $scope.columnToOrder, $scope.reverse);
 
        if ($scope.reverse) iconName = 'glyphicon glyphicon-chevron-up';
        else iconName = 'glyphicon glyphicon-chevron-down';
 
        if (sortBy === 'EmpId') {
            $scope.Header[0] = iconName;
        } else if (sortBy === 'name') {
            $scope.Header[1] = iconName;
        } else {
            $scope.Header[2] = iconName;
        }
 
        $scope.reverse = !$scope.reverse;
 
        $scope.pagination();
    };
 
    //By Default sort ny Name
    

});

app.controller('pageThree', function($scope, $http, $filter, filteredListService) {

    $scope.allItems = [];
    $scope.searchText = '';
    $scope.req = false;
    $scope.loading = false;

    $scope.search = function () {
        $scope.filteredList = filteredListService.searched($scope.allItems, $scope.searchText);
 
        if ($scope.searchText == '') {
            $scope.filteredList = $scope.allItems;
        }
        $scope.pagination();
    }

    $scope.sendJson = function(){

        $scope.loading = true;
        $http.post($scope.urlBase+"getAllRequisionPost", {cveDependencia: $scope.cveDependencia}).then(function(data, status) {
              console.log(data)
              $scope.loading = false;
              //$scope.sort('IdRequisicion');   
              //$scope.pagination();
        }).error(function(error){
           $scope.loading = false;
        })
    }

    $scope.read = function (workbook) {

                var headerNames = XLSX.utils.sheet_to_json( workbook.Sheets[workbook.SheetNames[0]], { header: 1 })[0];
                var data = XLSX.utils.sheet_to_json( workbook.Sheets[workbook.SheetNames[0]]);

                //console.log(headerNames);
                //console.log(data);
                for (var i = 0; i < data.length; i++) {

                    $scope.allItems[i] = {
                                  "Folio": (i + 1),
                                  "Tipo": null,
                                  "OficioSindicato": null,
                                  "Dependencia": null,
                                  "Direccion": null,
                                  "PuestoNominal": null,
                                  "puestoNominalPropuesto": null,
                                  "PuestoFuncional": null,
                                  "PuestoFuncionalPropuesto": null,
                                  "Motivo": null,
                                  "TipoPuesto": null,
                                  "ExperienciaRequerida": null,
                                  "FuncionesARealizar": null,
                                  "JefeInmediato": null,
                                  "CargoJefe": null,
                                  "CoordinadorAdmvo": null,
                                  "Secretario": null,
                                  "Capturista": null,
                                  "CorreoCapt": null,
                                  "Telefono": null,
                                  "Estatus": 1,
                                  "usrAcceso": 'sie',
                                  "items": []
                                };

                    Object.keys(data[i]).forEach(function(key) {

                        if(key.includes("ApMaterno") || key.includes("ApPaterno") || key.includes("Nombre") || key.includes("NoControl")){
                            var num = key.replace('ApMaterno','');
                            num = num.replace('ApPaterno','');
                            num = num.replace('Nombre','');
                            num = num.replace('NoControl','');
                            console.log(num);
                            if($scope.allItems[i]['items'][(parseInt(num) - 1)] == undefined){
                              $scope.allItems[i]['items'][(parseInt(num) - 1)] = {};
                            }

                            if($scope.allItems[i]['items'][(parseInt(num) - 1)].idConsecutivo == undefined){
                                $scope.allItems[i]['items'][(parseInt(num) - 1)].idConsecutivo = parseInt(num);
                            }

                            var str = key.replace(num,'');
                            str = str.replace(num,'');
                            str = str.replace(num,'');
                            str = str.replace(num,'');
                            $scope.allItems[i]['items'][(parseInt(num) - 1)][str] = data[i][key];
                        }else{
                            switch(key) {
                                case 'Tipo':
                                    $scope.allItems[i]['Tipo'] = data[i][key];
                                    break;
                                case 'NoOficio':
                                   $scope.allItems[i]['OficioSindicato'] = data[i][key];
                                    break;
                                case 'Direccion':
                                   $scope.allItems[i]['Direccion'] = data[i][key];
                                    break;
                                case 'PuestoNominal':
                                   $scope.allItems[i]['PuestoNominal'] = data[i][key];
                                    break;
                                case 'PuestoNominalPropuesto':
                                   $scope.allItems[i]['PuestoNominalPropuesto'] = data[i][key];
                                    break;
                                case 'PuestoFuncional':
                                   $scope.allItems[i]['PuestoFuncional'] = data[i][key];
                                    break;
                                case 'PuestoFuncionalPropuesto':
                                   $scope.allItems[i]['PuestoFuncionalPropuesto'] = data[i][key];
                                    break;
                                case 'MotivoSolicitud':
                                   $scope.allItems[i]['Motivo'] = data[i][key];
                                    break;
                                case 'TipoPuesto':
                                   $scope.allItems[i]['TipoPuesto'] = data[i][key];
                                    break;
                                case 'ExperienciaRequerida':
                                   $scope.allItems[i]['ExperienciaRequerida'] = data[i][key];
                                    break;
                                case 'FuncionesARealizar':
                                   $scope.allItems[i]['FuncionesARealizar'] = data[i][key];
                                    break;
                                case 'JefeInm':
                                   $scope.allItems[i]['JefeInmediato'] = data[i][key];
                                    break;
                                case 'CargoJefe':
                                   $scope.allItems[i]['CargoJefe'] = data[i][key];
                                    break;
                                case 'Coordinador':
                                   $scope.allItems[i]['CoordinadorAdmvo'] = data[i][key];
                                    break;
                                case 'Secretario':
                                   $scope.allItems[i]['Secretario'] = data[i][key];
                                    break;
                                case 'Capturista':
                                   $scope.allItems[i]['Capturista'] = data[i][key];
                                    break;
                                case 'Correo':
                                   $scope.allItems[i]['CorreoCapt'] = data[i][key];
                                    break;
                                case 'Telefono':
                                   $scope.allItems[i]['Telefono'] = data[i][key];
                                    break;
                            }
                            
                        }
                        /*console.log("Key = >" + key);
                        console.log("Value => " + data[row][key]);
                        console.log("===========================");*/
                    });
                };

                console.log($scope.allItems);
                $scope.req = true;
                $scope.$apply();
            }

            $scope.error = function (e) {
                console.log(e);
            }

});