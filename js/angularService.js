var app = angular.module('rdpApp', []);

app.service('ApiCall', ['$http', function ($http) {

    var urlBase = 'http://127.0.0.1:8000/api/v1';
    var result;

    // This is used for calling get methods from web api
    this.GetApiCall = function (controllerName, methodName) {
         result = $http.get(urlBase + controllerName + '/' + methodName).success(function (data, status) {
         result = (data);
        }).error(function (err) {
            console.log("Ocurió un error");
            console.log(err);
        });
        return result;
    };

    // This is used for calling post methods from web api with passing some data to the web api controller
    this.PostApiCall = function ( methodName, obj) {
        //debugger;
         result = $http.post(urlBase +'/' + methodName, obj)
             .then(function (response) {
              console.log(response.data)
              result = response;
             });
        return result;
    };

}]);

